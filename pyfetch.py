import subprocess as sp
cg = '\033[32;01m'
cy = '\033[33;01m'
cr = '\033[1;31m'
cb = '\033[34;01m'
cp = '\033[35m'
cn = '\033[0m'

user = sp.getoutput('echo $USER')
host = sp.getoutput('cat /etc/hostname')
line_counter = len(user + "@" + host)
line = ""
for i in range(line_counter):
    line = line + "-"
os = sp.getoutput('. /etc/os-release && echo $NAME')
res = sp.getoutput("xrandr --properties | grep current | awk '{print $8 $9 $10}' ")
kernel = sp.getoutput("uname -r")
shell = sp.getoutput("basename $SHELL")
arch = sp.getoutput("uname -m")
uptime = sp.getoutput("uptime -p")


title = (f"{cg}{user}{cn}@{cr}{host}{cn}")







print(f"""
            {cb}.:-============-:.{cn}
          {cb}.==-:-==++++++++++++=.{cn}                    {title}
          {cb}-==   -++++++++++++++-{cn}                    {cb}{line}{cn}
          {cb}-===-=+++++++++++++++={cn}                    {cp}OS:{cn} {os}
                    {cb}=++++++++++={cn}                    {cp}OS Arch:{cn} {arch}
   {cb}:--==============+++++++++++=  {cy}:::::::{cn}           {cp}Kernel:{cn} {kernel}
 {cb}.======+++++++++++++++++++++++=  {cy}:--------:{cn}        {cp}Res:{cn} {res}
{cb}.=====+++++++++++++++++++++++++-  {cy}:---------.{cn}       {cp}Uptime:{cn} {uptime.replace("up ", "")}
{cb}-===++++++++++++++++++++++++++=  {cy}:-----------{cn}       {cp}Shell:{cn} {shell}
{cb}==++++++++++=                   {cy}:------------{cn}
{cb}=+++++++++++:  {cy}..:::::::::::::--------------{cn}
{cb}=++++++++++: {cy}:::::::::----------------------{cn}
{cb}.++++++++++ {cy}.:::::::-----------------------.{cn}
 {cb}:+++++++++ {cy}.:::::------------------------.{cn}
  {cb}.-======= {cy}.:::--------{cn}
          {cy}.:------------..........{cn}
          {cy}.-----------------:::--:{cn}
          {cy}.-----------------   :-:{cn}
           {cy}:----------------:::--.{cn}
             {cy}..::-----------::..{cn}



""")
